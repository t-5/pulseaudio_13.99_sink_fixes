#!/bin/bash

sudo apt source pulseaudio && \
sudo apt install build-essential libsoxr-dev libsoxr0 python3-pyqt5 python3-dbus.mainloop.pyqt5 && \
sudo apt build-dep pulseaudio && \
sudo chmod 755 pulseaudio-??.* && \
cd pulseaudio-??.* && \
(sudo patch --forward -p1 <../sink-fixes.patch; exit 0) && \
sudo rm -f config.log && \
sudo dpkg-buildpackage -b -uc -us
