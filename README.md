# Pulseaudio 13.99.x from sources with fix for #1015

This is a small collection of scripts to build pulseaudio 13.99.x (included
in ubuntu 20.04, mint 20...) from the debian source files, while applying a
fix for an issue that prevented pulseaudio crossover rack to run correctly.

## Prerequisites

You will have to enable source package repositories before doing this!

## build.sh

Run ./build.sh to:

1. download source files
2. install build dependencies
3. apply patch
4. build .deb packages

## install.sh

Run ./install.sh to install built .deb packages.

You have to kill the running pulseaudio daemon (killall -KILL pulseaudio)
or reboot your machine for the newly installed version to to take over.

## hold.sh

Run ./hold.sh to hold updates for the pulseaudio package. This prevents
apt to retrieve updates from debian repositories and overwrite the package
we just built and installed.

## clean.sh

Run ./clean.sh to remove source packages, built .deb packages and other
build artefacts. After running this you should be left with the five shell
scripts you began with.

## unhold.sh

Run ./unhold.sh to remove the hold for the pulseaudio package and enable
updates via apt/debain repository again. Be aware that this will in turn
remove support for SOXR again unless your distro has catched up in the
meantime! This script also upgrades packages to resolve any conflicts
that may arise from installing built packages in the future.

## Have fun!
